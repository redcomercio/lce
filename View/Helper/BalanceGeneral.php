<?php

/**
 * LibreDTE
 * Copyright (C) SASCO SpA (https://sasco.cl)
 *
 * Este programa es software libre: usted puede redistribuirlo y/o
 * modificarlo bajo los términos de la Licencia Pública General Affero de GNU
 * publicada por la Fundación para el Software Libre, ya sea la versión
 * 3 de la Licencia, o (a su elección) cualquier versión posterior de la
 * misma.
 *
 * Este programa se distribuye con la esperanza de que sea útil, pero
 * SIN GARANTÍA ALGUNA; ni siquiera la garantía implícita
 * MERCANTIL o de APTITUD PARA UN PROPÓSITO DETERMINADO.
 * Consulte los detalles de la Licencia Pública General Affero de GNU para
 * obtener una información más detallada.
 *
 * Debería haber recibido una copia de la Licencia Pública General Affero de GNU
 * junto a este programa.
 * En caso contrario, consulte <http://www.gnu.org/licenses/agpl.html>.
 */

namespace website\Lce;

/**
 * Clase para generar PDF con el balance general
 * @author Esteban De La Fuente Rubio, DeLaF (esteban[at]sasco.cl)
 * @version 2016-10-19
 */
class View_Helper_BalanceGeneral extends \website\View_Helper_PDF
{

    public function __construct()
    {
        parent::__construct('L');
    }

    public function agregar($periodo, $datos)
    {
        // agregar página
        $this->titulo = 'Balance tributario año comercial '.$periodo;
        $hasta = $periodo < date('Y') ? '31/12/'.$periodo : date('d/m/y');
        $this->subtitulo = '01/01/'.$periodo.' al '.$hasta;
        $this->startPageGroup();
        $this->AddPage();
        // preparar datos
        extract($datos);
        foreach ($balance as &$cuenta) {
            foreach ($cuenta as &$valor) {
                if (is_numeric($valor)) {
                    $valor = num($valor);
                }
            }
        }
        foreach ($sumas_parciales as &$valor) {
            if (is_numeric($valor)) {
                $valor = num($valor);
            }
        }
        foreach ($resultados as &$valor) {
            if (is_numeric($valor)) {
                $valor = num($valor);
            }
        }
        foreach ($suma_total as &$valor) {
            if (is_numeric($valor)) {
                $valor = num($valor);
            }
        }
        // agregar tabla con balance
        $titulos = ['Cuenta', 'Débitos', 'Créditos', 'Saldo deud.', 'Saldo acre.', 'Activo', 'Pasivo', 'Pérdidas', 'Ganancias'];
        array_unshift ($sumas_parciales, '<span style="text-align:right"><strong>Sumas parciales</strong></span>');
        $resultados = ['<span style="text-align:right"><strong>Resultados</strong></span>','','','',''] + $resultados;
        array_unshift ($suma_total, '<span style="text-align:right"><strong>Suma Total</strong></span>');
        $balance[] = $sumas_parciales;
        $balance[] = $resultados;
        $balance[] = $suma_total;
        $this->SetFont('helvetica', '', 9);
        $this->addTable($titulos, $balance, ['width'=>[75, 22, 22, 22, 22, 22, 22, 22, 22], 'align'=>['left', 'right', 'right', 'right', 'right', 'right', 'right', 'right', 'right']], true);
        $this->Ln();
        // agregar firmas
        $y = $this->getY()+3;
        $this->MultiTexto('De acuerdo a lo estipulado en el artículo N° 100 del Código Tributario, dejo constancia que los datos proporcionados para los asientos contables que dieron origen al presente balance son fidedignos.', 140, $y);
        $y = $this->getY()+20;
        $contador = '___________________________'."\n";
        if ($this->Contribuyente->config_contabilidad_contador_nombre)
            $contador .= $this->Contribuyente->config_contabilidad_contador_nombre."\n";
        if ($this->Contribuyente->config_contabilidad_contador_rut)
            $contador .= $this->Contribuyente->config_contabilidad_contador_rut."\n";
        $contador .= 'Contador'."\n";
        $this->MultiTexto($contador, null, $y, 'C', 150);
        $this->MultiTexto('_________________________'."\n".$this->Contribuyente->razon_social."\n".$this->Contribuyente->getRUT(), 120, $y, 'C');
    }

}
